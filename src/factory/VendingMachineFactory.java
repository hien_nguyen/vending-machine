package factory;

import constant.VendingMachineType;
import service.VendingMachine;
import service.WaterVendingMachineImpl;

/**
 * Factory class, I create this due to the Factory Pattern Design
 * Objective: We can have multiple types of Vending Machine (Water || Snack || etc.)
 * This Factory will return the type of vending machine you would like to get
 * @author HienNg
 *
 */
public class VendingMachineFactory {
	
	public static VendingMachine getVendingMachine(VendingMachineType type) {
		switch(type) {
			case WATER:
				return new WaterVendingMachineImpl();
			case SNACK:
				// return new SnackVendingMachine();
				break;
			case INSTAX:
				// return new InstaxVendingMachine();
				break;
		}
		return new WaterVendingMachineImpl();
	}
}
