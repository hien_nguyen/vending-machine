package test;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import constant.Coin;
import constant.Item;
import constant.VendingMachineType;
import exception.SoldoutItemException;
import factory.VendingMachineFactory;
import service.VendingMachine;

public class VendingMachineTest {
	private static VendingMachine waterVm;
	
	@Before
	public void initializeWaterVendingMachine() {
		waterVm = VendingMachineFactory.getVendingMachine(VendingMachineType.WATER);
	}
	
	@Test
	public void testBuyCoke() {
		Map<Item, List<Coin>> expectedResult = new HashMap<Item, List<Coin>>();

		// expect no coins return
		waterVm.insertCoins(Coin.QUARTER);
		expectedResult.put(Item.COKE, new ArrayList<Coin>());
		assertEquals(expectedResult, waterVm.selectItem(Item.COKE));
		
		// expect coins return
		waterVm.insertCoins(Coin.DIME);
		waterVm.insertCoins(Coin.DIME);
		waterVm.insertCoins(Coin.DIME);
		expectedResult.put(Item.COKE, Arrays.asList(Coin.NICKLE));
		assertEquals(expectedResult, waterVm.selectItem(Item.COKE));
	}
	
	@Test
	public void testBuyPepsi() {
		Map<Item, List<Coin>> expectedResult = new HashMap<Item, List<Coin>>();
		
		// expect no coins return
		waterVm.insertCoins(Coin.QUARTER);
		waterVm.insertCoins(Coin.DIME);
		expectedResult.put(Item.PEPSI, new ArrayList<Coin>());
		assertEquals(expectedResult, waterVm.selectItem(Item.PEPSI));
		
		// expect coins return
		waterVm.insertCoins(Coin.QUARTER);
		waterVm.insertCoins(Coin.QUARTER);
		expectedResult.put(Item.PEPSI, Arrays.asList(Coin.DIME, Coin.NICKLE));
		assertEquals(expectedResult, waterVm.selectItem(Item.PEPSI));
	}
	
	@Test(expected = SoldoutItemException.class)
	public void testBuySoldoutItem() {
		for (int index = 0; index <= 10; index++) {
			waterVm.insertCoins(Coin.QUARTER);
			waterVm.selectItem(Item.COKE);
		}
		waterVm.cancel();
	}
}
