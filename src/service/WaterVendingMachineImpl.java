package service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import constant.Coin;
import constant.Inventory;
import constant.Item;
import exception.InsufficientCoinException;
import exception.InvalidCoinException;
import exception.RunningOutCoinException;
import exception.SoldoutItemException;

public class WaterVendingMachineImpl implements VendingMachine {
	private Inventory<Coin> coins = new Inventory<Coin>();
	private Inventory<Item> items = new Inventory<Item>();
	
	private int totalInsertAmount;
	
	/**
	 * Initialize the vending machine with 10 coins each and 10 items each
	 */
	public WaterVendingMachineImpl() {
		for (Coin coin : Coin.values())
			coins.put(coin, 10);
		for (Item item : Item.values())
			items.put(item, 10);
	}

	@Override
	public void insertCoins(Coin coin) {
		if (!(coin instanceof Coin)) throw new InvalidCoinException(coin.getValue());
		totalInsertAmount += coin.getValue();
		coins.addItem(coin);
	}

	@Override
	public Map<Item, List<Coin>> selectItem(Item item) {
		if (!items.hasItem(item))
			throw new SoldoutItemException(item);
		if (item.getPrice() > totalInsertAmount)
			throw new InsufficientCoinException(item.getPrice() - totalInsertAmount);
		
		int remaining = totalInsertAmount - item.getPrice();
		List<Coin> refundCoins = refund(remaining);
		
		Map<Item, List<Coin>> map = new HashMap<>(2);
		
		map.put(items.deductItem(item), refundCoins);
		totalInsertAmount = 0;
		return map;
	}

	@Override
	public List<Coin> cancel() {
		// TODO Auto-generated method stub
		List<Coin> coins = refund(totalInsertAmount);
		totalInsertAmount = 0;
		return coins;
	}

	@Override
	public void reset() {
		// TODO Auto-generated method stub
		coins.clear();
		items.clear();
		totalInsertAmount = 0;
	}

	/**
	 * Get the min amount of coins return possible
	 * or just return from top down
	 */
	private List<Coin> refund(int amount) {
		List<Coin> refundCoins = new ArrayList<Coin>();
		if (amount == 0) return refundCoins;
		
		// Loop through biggest coin
		for (Coin coin : Coin.values()) {
			if (amount == 0) break;
			while (amount >= coin.getValue() && coins.hasItem(coin)) {
				refundCoins.add(coins.deductItem(coin));
				amount -= coin.getValue();
			}
		}
		if (amount > 0) throw new RunningOutCoinException();
		return refundCoins;
	}
	
}
