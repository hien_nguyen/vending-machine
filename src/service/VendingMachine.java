package service;

import java.util.List;
import java.util.Map;

import constant.Coin;
import constant.Item;

public interface VendingMachine {
	public void insertCoins(Coin coin);
	public Map<Item, List<Coin>> selectItem(Item item);
	public List<Coin> cancel();
	public void reset();
}
