package exception;

public class InvalidCoinException extends RuntimeException {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -4824872505995786294L;
	
	private int value;
	
	public InvalidCoinException(int value) {
		this.value = value;
	}
	
	@Override 
	public String getMessage() { 
		return String.format("This coin %d is not supported", value);
	}
}
