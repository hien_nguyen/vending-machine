package exception;

public class RunningOutCoinException extends RuntimeException {
	
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 6407597639705589354L;

	@Override 
	public String getMessage() { 
		return String.format("Vending Machine runs out of coins");
	}
}
