package exception;

public class InsufficientCoinException extends RuntimeException {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = 7868742891749838351L;
	
	private int remaining;
	
	public InsufficientCoinException(int remaining) {
		this.remaining = remaining;
	}
	
	@Override 
	public String getMessage() { 
		return String.format("Please insert %d more", remaining);
	}
}
