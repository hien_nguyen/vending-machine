package exception;

import constant.Item;

public class SoldoutItemException extends RuntimeException {
	/**
	 * serialVersionUID
	 */
	private static final long serialVersionUID = -3261531466752200229L;
	
	private Item item;
	
	public SoldoutItemException(Item item) {
		this.item = item;
	}
	
	@Override
	public String getMessage() {
		return String.format("Item %s is sold out.", item.getName());
	}
}
