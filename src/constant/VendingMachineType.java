package constant;

public enum VendingMachineType {
	WATER,
	SNACK,
	INSTAX
}
