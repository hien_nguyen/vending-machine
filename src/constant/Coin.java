package constant;

/**
 * Supported by Vending Machine
 * @author HienNg
 *
 */
public enum Coin {
	QUARTER(25),
	DIME(10),
	NICKLE(5),
	PENNY(1);
	
	private int value;
	
	public int getValue() {
		return value;
	}
	
	private Coin(int value) {
		this.value = value;
	}
}
