package constant;

import java.util.HashMap;
import java.util.Map;

/**
 * An adapter over Map
 * Objective: Stores the Coin and Item
 * @author HienNg
 *
 */
public class Inventory<T> {
	private Map<T, Integer> inventory = new HashMap<T, Integer>();
	
	public void put(T t, int number) {
		inventory.put(t, number);
	}
	
	public void addItem(T t) {
		if (inventory.containsKey(t)) {
			int invCount = inventory.get(t);
			this.put(t, invCount+1);
		} else {
			this.put(t, 1);
		}
	}
	
	public T deductItem(T t) {
		if (inventory.containsKey(t)) {
			int invCount = inventory.get(t);
			this.put(t, invCount-1);
		}
		return t;
	}
	
	public boolean hasItem(T t) {
		return inventory.containsKey(t) && inventory.get(t) > 0;
	}
	
	public void clear() {
		inventory.clear();
	}
}
